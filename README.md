# RobotFrameworkSetup

robotframework一键式安装包

# 包含的Robotframework库
```
robotframework==3.0.4
robotframework-appiumlibrary==1.4.6
robotframework-autoitlibrary==1.2.2
robotframework-csvlibrary==0.0.2
robotframework-databaselibrary==1.0.1
robotframework-debuglibrary==1.1.4
robotframework-excellibrary==0.0.2
robotframework-faker==4.2.0
robotframework-requests==0.4.8
robotframework-ride==1.5.2.1
robotframework-seleniumlibrary==3.1.1
robotframework-openstflibrary==0.1.1
```
# 支持的操作系统
|平台|位数|是否支持|
|---|-----|-------|
|win7|64位|是|
|win10|64位|是|
|win7|32位|是|